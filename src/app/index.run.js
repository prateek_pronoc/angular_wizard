(function() {
  'use strict';

  angular
    .module('angularWizard')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
