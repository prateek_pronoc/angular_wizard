(function() {
  'use strict';

  angular
    .module('angularWizard', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap']);

})();
