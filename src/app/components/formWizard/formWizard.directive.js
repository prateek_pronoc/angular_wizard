(function() {
    'use strict';

    angular
        .module('angularWizard')
        .directive('formWizard', formWizard);

    /* @ngInject */
    function formWizard() {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            link: link,
            restrict: 'E',
            replace: true,
            templateUrl: 'app/components/formWizard/formWizard.tpl.html',
            controller: function($scope) {
                $scope.currentStep = 1;
                $scope.next = next;
                $scope.prev = prev;
                $scope.uiObj = {};
                $scope.validateObj = {};
                $scope.goToFirst = goToFirst;

                function next() {
                   
                    if ($scope.currentStep === 4) {
                        $scope.currentStep = 1;
                    }
                    $scope.currentStep = $scope.currentStep + 1;
                }

                function prev() {
                    $scope.currentStep = $scope.currentStep - 1;
                }

                function goToFirst() {
                    $scope.currentStep = 1;
                }

            }
        };
        return directive;

        function link() {}
    }
})();
