(function() {
    'use strict';

    angular
        .module('angularWizard')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url: '/',
                views: {

                    '@': {
                        templateUrl: 'app/main/main.html',
                        controller: 'MainController as main'
                    }
                }
            });
    }

})();
